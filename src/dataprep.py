from utils.utils import loadimgs
import os
import pickle

train_folder = '/Users/goddard/cernbox/Gitlab_Projects/omniglot/python/images_background'
val_folder = '/Users/goddard/cernbox/Gitlab_Projects/omniglot/python/images_evaluation'
save_path = '../data'

print(os.listdir(val_folder))

X,y,c = loadimgs(train_folder)

with open(os.path.join(save_path,"train.pickle"), "wb") as f:
    pickle.dump((X,c),f)

Xval, yval, cval = loadimgs(val_folder)

with open(os.path.join(save_path,"val.pickle"), "wb") as f:
    pickle.dump((Xval,cval),f)