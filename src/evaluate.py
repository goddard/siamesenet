import os
from utils.utils import get_siamese_model, test_oneshot, test_nn_accuracy, loader
import warnings
import numpy as np
import pickle

warnings.simplefilter(action='ignore', category=FutureWarning)
model_path = '../weights'
save_path = '../data'

def main():
    model = get_siamese_model((105, 105, 1))
    model.load_weights(os.path.join(model_path, "weights.200.h5"))
    ways = np.arange(1,20,2)
    resume =  False
    trials = 50
    val_accs, train_accs, nn_accs = [], [], []
    Xtrain, train_classes, Xval, val_classes = loader(save_path)
    for N in ways:
        val_accs.append(test_oneshot(model, Xtrain, Xval, train_classes, val_classes, N, trials, "val", verbose=True))
        train_accs.append(test_oneshot(model, Xtrain, Xval, train_classes, val_classes, N, trials, "train", verbose=True))
        nn_acc = test_nn_accuracy(N, trials, Xtrain, Xval, train_classes, val_classes)
        nn_accs.append(nn_acc)
        print("NN Accuracy = ", nn_acc)
        print(
            "---------------------------------------------------------------------------------------------------------------")

    with open(os.path.join(save_path,"accuracies.pickle"), "wb") as f:
        pickle.dump((val_accs,train_accs,nn_accs),f)

if __name__ == "__main__":
    main()


