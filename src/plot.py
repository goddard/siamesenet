import os
from utils.utils import make_oneshot_task, loader, plot_oneshot_task, plot_results
import warnings
import pickle
import numpy as np

warnings.simplefilter(action='ignore', category=FutureWarning)
model_path = '../weights'
save_path = '../data'

def main():
    ways = np.arange(1, 20, 2)
    with open(os.path.join(save_path, "accuracies.pickle"), "rb") as f:
        (val_accs, train_accs, nn_accs) = pickle.load(f)

    Xtrain, train_classes, Xval, val_classes = loader(save_path)
    pairs, targets = make_oneshot_task(16, Xtrain, Xval, train_classes, val_classes, "train", "Sanskrit")
    plot_oneshot_task(pairs, save_path)

    plot_results(ways, val_accs, train_accs, nn_accs, Xtrain, Xval, train_classes, val_classes, save_path)

if __name__ == "__main__":
    main()


