import os
import pickle
from utils.utils import get_siamese_model, test_oneshot, get_batch, loader
from keras.optimizers import Adam
import time

train_folder = '/Users/goddard/cernbox/Gitlab_Projects/omniglot/python/images_evaluation'
val_folder = '/Users/goddard/cernbox/Gitlab_Projects/omniglot/python/images_background'
save_path = '../data'
model_path = '../weights'

# Hyper parameters
evaluate_every = 20 # interval for evaluating on one-shot tasks
batch_size = 32
n_iter = 200 # No. of training iterations
N_way = 20 # how many classes for testing one-shot tasks
n_val = 25 # how many one-shot tasks to validate on

def main():
    best = -1
    model = get_siamese_model((105, 105, 1))
    model.summary()
    optimizer = Adam(lr=0.00006)
    model.compile(loss="binary_crossentropy", optimizer=optimizer)

    Xtrain, train_classes, Xval, val_classes = loader(save_path)

    print("Validation alphabets:", end="\n\n")
    print(list(val_classes.keys()))

    print("Starting training process!")
    print("-------------------------------------")
    t_start = time.time()

    for i in range(1, n_iter + 1):
        (inputs, targets) = get_batch(batch_size, Xtrain, Xval, train_classes, val_classes)
        loss = model.train_on_batch(inputs, targets)
        if i % evaluate_every == 0:
            print("\n ------------- \n")
            print("Time for {0} iterations: {1} mins".format(i, (time.time() - t_start) / 60.0))
            print("Train Loss: {0}".format(loss))
            val_acc = test_oneshot(model, Xtrain, Xval, train_classes, val_classes, N_way, n_val, verbose=True)
            model.save_weights(os.path.join(model_path, 'weights.{}.h5'.format(i)))
            if val_acc >= best:
                print("Current best: {0}, previous best: {1}".format(val_acc, best))
                best = val_acc

if __name__ == "__main__":
    main()


